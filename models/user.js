const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');

const UserSchema = mongoose.Schema({
    name: {
        type: String
    },
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    location: {
        lat: {
            type: Number
        },
        lon: {
            type: Number
        }
    },
    isTechnician: {
        type: Boolean,
        required: true
    },
    type: {
        type: String,
        enum: ["Battery", "Tire", "Painting", "Engine Repairing", "Air Condition"]
    },
    rating: {
        type: Number,
        default: 0.0
    }
})

const User = module.exports = mongoose.model('User', UserSchema)

module.exports.getAllUsers = function (callback) {
    User.find(callback)
}

// module.exports.getAllJobs = function (user, callback) {
//     const query = { username: username };
//     User.find(callback)
// }

module.exports.getUserById = function (id, callback) {
    User.findById(id, callback)
}

module.exports.getUserByUsername = function (username, callback) {
    const query = { username: username };
    User.findOne(query, callback)
}

module.exports.addUser = function (newUser, callback) {
    bcrypt.genSalt(10, function (err, salt) {
        bcrypt.hash(newUser.password, salt, function (err, hash) {
            if (err) console.log('REGISTER ERROR: ' + err)
            newUser.password = hash
            newUser.save(callback)
        })
    })
}

module.exports.comparePassword = function (candidatePassword, hash, callback) {
    bcrypt.compare(candidatePassword, hash, (err, isMacth) => {
        if (err) { console.log('User model - wrong password: ' + err); throw err }
        callback(null, isMacth)
    })
}