const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const User = require('../models/user');

const Technician = module.exports = mongoose.model('User', User.UserSchema)

module.exports.getAllUsers = function (type, callback) {
    const condition = { isTechnician: true, type: type }
    Technician.find(condition).sort('-rating').exec(callback)
}

module.exports.getUserById = function (id, callback) {
    Technician.findById(id, callback)
}

module.exports.getUserByUsername = function (username, callback) {
    const query = { username: username };
    Technician.findOne(query, callback)
}

module.exports.updateTechnicianRating = function (techid, rating, callback) {
    Technician.findByIdAndUpdate(techid, { 
        $set: {
            rating: rating
        }
    }, callback);
}

module.exports.getUserByUserId = function (user_id, callback) {
    const query = { _id: user_id };
    Technician.findOne(query, callback)
}

module.exports.addTechnician = function (newUser, callback) {
    bcrypt.genSalt(10, function (err, salt) {
        bcrypt.hash(newUser.password, salt, function (err, hash) {
            if (err) console.log('REGISTER ERROR: ' + err)
            newUser.password = hash
            newUser.save(callback)
        })
    })
}

module.exports.comparePassword = function (candidatePassword, hash, callback) {
    bcrypt.compare(candidatePassword, hash, (err, isMacth) => {
        if (err) { console.log('User model - wrong password: ' + err); throw err }
        callback(null, isMacth)
    })
}