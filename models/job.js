const Technician = require('../models/technician');

const mongoose = require('mongoose');

const JobSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    jobDescription: {
        type: String,
    },
    createdDate: {
        type: String,
        default: new Date(Date.now()).toISOString(),
    },
    status: {
        type: String,
        enum: ["Awaiting", "In Progress", "Done"]
    },
    feedback: {
        type: String,
        default: "-"
    },
    rating: {
        type: Number,
        default: 0.0
    },
    lat: {
        type: String
    },
    lon: {
        type: String
    },
    address: {
        type: String
    },
    technician: {
        type: mongoose.Schema.ObjectId,
        ref: 'Technician'
    },
    reportingUser: {
        type: mongoose.Schema.ObjectId,
        ref: 'User'
    },
    // reportingUser: {userId: String, userName: String}
})

const Job = module.exports = mongoose.model('Job', JobSchema)

module.exports.getAllJobs = function (user, callback) {
    // Job.find(callback)
    const condition = {
        technician:  { $elemMatch: {id: user.id.toString()} }
    }

    Job.find(condition, callback)
}

module.exports.getRatedJobs = function (user, callback) {
    // Job.find(callback)
    const condition = {
        technician: user.id,
        rating: { $gt : 0}
    }

    Job.find(condition, callback)
}

module.exports.getAllUserJobs = function (user, callback) {
    // Job.find(callback)
    const condition = {
        reportingUser: user.id.toString()
    }
    console.log(condition)
    Job.find(condition, callback)
}

module.exports.getAllTechnicianJobs = function (user, callback) {
    // Job.find(callback)
    // const condition = {
    //     technician:  { $elemMatch: {id: user.id.toString()} }
    // }
    const condition = {
        technician:  user.id.toString()
    }
    console.log(condition)
    Job.find(condition).sort('-createdDate').exec(callback)
}

module.exports.getAllTechnicianJobsForRating = function (techid, callback) {
    const condition = {
        technician: techid,
        rating: { $gt : 0}
    }

    Job.find(condition, callback)
}

module.exports.insertJob = function (technicianId, reportingUser, res, newJob, callback) {
    Technician.getUserById(technicianId, (err, technician) => {
        if (err) {
            console.log('Technician not found' + err)
            return res.json({ success: false, message: "Technician not found with error: "+err })
        }
        // console.log('User found: '+user);
        if (!technician) {
            return res.json({ success: false, message: "Technician not found" })
        } else {

            // reportingUser.jobs.push(newJob)
            // reportingUser.save()

            // technician.jobs.push(newJob)
            // technician.save()

            newJob.reportingUser = reportingUser
            newJob.technician = technician
            newJob.save()

            res.json({success: true})
        }
    })
}

module.exports.updateFeedback = function (job, callback) {
    Job.findByIdAndUpdate(job.jobId, { 
        $set: { 
            feedback: job.feedback,
            rating: job.rating
        }
    }, callback);
}

module.exports.updateDetails = function (job, callback) {
    Job.findByIdAndUpdate(job.jobId, { 
        $set: { 
            name: job.name,
            status: job.status,
            jobDescription: job.jobDescription,
        }
    }, callback);
}