import { Injectable } from '@angular/core';
import { Http, Headers, Response, HttpModule} from '@angular/http';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';

import 'rxjs/add/operator/map'
import { Observable }   from 'rxjs/Observable';
import { AuthService } from './auth.service';
import { Job } from '../models/job.model';

@Injectable()
export class JobService {

  constructor(
    private http: Http, 
    private authService: AuthService,
    private httpClient: HttpClient
  ) { }

  getAllJobs(): Observable<Job[]> {
    this.authService.loadToken()
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': this.authService.authToken
      })
    };
    console.log("getting all jobs")
    // return this.httpClient.get<Job[]>('http://localhost:3000/technicians/alljobs', httpOptions)
    return this.httpClient.get<Job[]>('technicians/alljobs', httpOptions)
  }

  getAllRatedJobs(): Observable<Job[]> {
    this.authService.loadToken()
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': this.authService.authToken
      })
    };
    console.log("getting all jobs")
    // return this.httpClient.get<Job[]>('http://localhost:3000/jobs/ratedjobs', httpOptions)
    return this.httpClient.get<Job[]>('jobs/ratedjobs', httpOptions)
  }

  editJob(job) {
    // console.log("ediding job id: "+job._id)
    this.authService.loadToken()
    let headers = new Headers();
    headers.append('Authorization',this.authService.authToken)
    headers.append('Content-Type','application/json')
    // return this.http.post('http://localhost:3000/technicians/updatejobstatus', job, {headers: headers}).map(res => res.json())
    return this.http.post('technicians/updatejobstatus', job,{headers: headers}).map(res => res.json())
  }

}
