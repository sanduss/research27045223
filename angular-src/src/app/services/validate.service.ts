import { Injectable } from '@angular/core';

@Injectable()
export class ValidateService {

  constructor() { }

  validateRegisterDetails(user) {
    if (user.name == undefined || user.username == undefined || user.password == undefined || user.address == undefined || user.phone == undefined) {
      return false
    } else {
      return true
    }
  }

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  validateLoginDetails(login) {
    if (login.username == undefined || login.password == undefined) {
      window.alert('Please fill required fields')
      return false
    } else {
      return true
    }
  }

  validateAddProjectDetails(newProject) {
    if (newProject.name == undefined || newProject.projectDescription == undefined || newProject.projectCode == undefined) {
      return false
    } else {
      return true
    }
  }

  validateCreateSprint(sprint) {
    if (sprint.sprintName == undefined || sprint.sprintStartingDate == undefined || sprint.sprintEndDate == undefined) {
      return false
    } else {
      return true
    }
  }

  validateCreateTask(task) {
    if (task.name == undefined || task.taskDescription == undefined || task.assignedUser.userId == undefined || task.taskNumber == undefined) {
      return false
    } else {
      return true
    }
  }
}
