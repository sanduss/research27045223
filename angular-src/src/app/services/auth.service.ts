import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map'
import { log } from 'util';
import { tokenNotExpired } from 'angular2-jwt'
import { Router, CanActivate } from '@angular/router';

@Injectable()
export class AuthService { //implements CanActivate 
  authToken: any
  user: any
  constructor(private http: Http, private router: Router) { }

  registerUser(user) {
    let headers = new Headers();
    headers.append('Content-Type','application/json')
    // return this.http.post('http://localhost:3000/technicians/registertechnician', user, {headers: headers}).map(res => res.json())
    return this.http.post('technicians/registertechnician', user, {headers: headers}).map(res => res.json())
  }

  loginUser(login){
    let headers = new Headers();
    headers.append('Content-Type','application/json')
    // return this.http.post('http://localhost:3000/technicians/authenticate', login, {headers: headers}).map(res => res.json())
    return this.http.post('technicians/authenticate', login, {headers: headers}).map(res => res.json())
  }

  getProfile() {
    let headers = new Headers();
    this.loadToken()
    headers.append('Authorization',this.authToken)
    headers.append('Content-Type','application/json')
    // return this.http.get('http://localhost:3000/users/profile', {headers: headers}).map(res => res.json())
    return this.http.get('users/profile', {headers: headers}).map(res => res.json())
  }

  loadToken() {
    const token = localStorage.getItem('id_token') //id_token
    this.authToken = token
    this.user = this.user
  }

  storeUserData(token, user) {
    localStorage.setItem('id_token',token)
    localStorage.setItem('user',JSON.stringify(user))
    this.authToken = token
    this.user = user
  }

  logout() {
    let headers = new Headers();
    headers.append('Content-Type','application/json')
    this.http.post('logout', {headers: headers}).map(res => res.json())
    this.http.post('users/logout', {headers: headers}).map(res => res.json())
    // this.http.post('http://localhost:3000/users/logout', {headers: headers}).map(res => res.json())
    this.http.post('users/logout', {headers: headers}).map(res => res.json())
    this.authToken = null
    this.user = null
    localStorage.clear()
  }

  loggedIn() {
    return tokenNotExpired('id_token')
  }
}
