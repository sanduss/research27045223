import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule, MatNativeDateModule } from '@angular/material'
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule } from '@angular/material/menu'
import { MatCardModule } from '@angular/material/card'
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatIconModule } from '@angular/material/icon'
import { MatInputModule } from '@angular/material/input'
// import { MatSnackBarModule } from '@angular/material/snack-bar'
import { MatGridListModule } from '@angular/material/grid-list'
import { MatDialogModule } from '@angular/material/dialog'
import { MatTableModule } from '@angular/material/table';
import { MatListModule } from '@angular/material/list';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion'
import { MatDatepickerModule, MatDatepickerToggle } from '@angular/material/datepicker';
import { MatSelectModule } from '@angular/material/select'

import {} from 'hammerjs';

// import { MatSnackBar } from '@angular/material/snack-bar';


@NgModule ({
    imports: [
        MatButtonModule, 
        MatToolbarModule,
        MatMenuModule,
        MatCardModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatGridListModule,
        MatDialogModule,
        MatTableModule,
        MatDividerModule,
        MatListModule,
        MatExpansionModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatSelectModule,
    ],
    exports: [
        MatButtonModule, 
        MatToolbarModule,
        MatMenuModule,
        MatCardModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatGridListModule,
        MatDialogModule,
        MatTableModule,
        MatDividerModule,
        MatListModule,
        MatExpansionModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatSelectModule
    ]
})

export class MaterialModule {}