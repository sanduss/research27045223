import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClient, HttpHandler, HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes, Route } from '@angular/router';
import {  } from '@angular/animations';

// Material
import { MaterialModule } from './material.module';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LoginComponent } from './components/login/login.component';
import { ProfileComponent } from './components/profile/profile.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { FooterViewComponent } from './components/footer-view/footer-view.component';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";

import { AuthGuard } from './guards/auth.guard';
import { ValidateService } from './services/validate.service';
import { AuthService } from './services/auth.service';
import { SimpleDialogComponent } from './components/simple-dialog/simple-dialog.component';
import { NotificationsComponent } from './components/notifications/notifications.component';
import { JobService } from './services/job.service';
import { JobDetailsComponent } from './components/job-details/job-details.component';

const appRoutes: Routes = [
  {path:'', component:HomeComponent},
  {path:'register', component:RegisterComponent},
  {path:'login', component:LoginComponent},
  {path:'jobs', component:NotificationsComponent, canActivate:[AuthGuard]},
  {path:'jobdetail', component:JobDetailsComponent, canActivate:[AuthGuard]},
  {path:'job/:id/edit', component:JobDetailsComponent, canActivate:[AuthGuard]},
  {path:'profile', component:ProfileComponent},
]

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    SimpleDialogComponent,
    FooterViewComponent,
    NotificationsComponent,
    JobDetailsComponent,
    ProfileComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    GooglePlaceModule
  ],
  providers: [
    ValidateService,
    AuthService,
    AuthGuard,
    HttpClientModule,
    JobService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
