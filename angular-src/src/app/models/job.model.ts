export interface Job {
    _id: String,
    title: String;
    createdDate: String;
    isActive: Boolean;
    notiDescription: String;
    rating: Number,
    reportingUser: any
}

export class Job {
    constructor() {

    }
}