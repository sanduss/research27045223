import { Component, OnInit } from '@angular/core';
import { PassThrough } from 'stream';
import { MaterialModule } from '../../material.module';

import { ValidateService } from '../../services/validate.service'
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { Address } from '../../models/address';


// import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  name: string
  username: string
  phone: string
  address: string
  password: string
  selectedType: string
  lat: Number
  lon: Number
  
  types = [
    {value: 'Battery', viewValue: 'Battery'},
    {value: 'Tire', viewValue: 'Tire'},
    {value: 'Painting', viewValue: 'Painting'},
    {value: 'Engine Repairing', viewValue: 'Engine Repairing'},
    {value: 'Air Condition', viewValue: 'Air Condition'},
  ];

  constructor(
    private vaidateService: ValidateService, 
    private authService: AuthService, 
    private router: Router
  ) { }

  ngOnInit() {
    console.log("It's new year season. Happy new year!")
  }

  public onAddressChange(address: Address) {
    console.log(address.geometry.location.lng());
    console.log(address.geometry.location.lat());
    console.log(address.geometry.location.toJSON());
    console.log(address.geometry.viewport.getNorthEast());
    // this.address = address.adr_address
    this.lat = address.geometry.location.lat()
    this.lon = address.geometry.location.lng()
}

  onRegisterSubmit() {
    const user =  {
      name: this.name,
      username: this.username,
      password: this.password,
      phone: this.phone,
      address: this.address,
      type: this.selectedType,
      lat: this.lat,
      lon: this.lon
    }
    
    // required fields
    if (!this.vaidateService.validateRegisterDetails(user)) {
      window.alert('Please fill in all fields')
      return false
    }
    
    // register user
    this.authService.registerUser(user).subscribe(data =>  {
      if (data.success) {
        window.alert('Register success')
        this.router.navigate(['/login'])
      } else {
        window.alert('Register failed')
      }
    })
  }
}
