import { Component, OnInit, Inject, ChangeDetectorRef } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { } from '../../material.module';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { JobDetailsComponent } from '../job-details/job-details.component';
import { JobService } from '../../services/job.service';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { ValidateService } from '../../services/validate.service';
import { Observable } from 'rxjs/Observable'
import { Job } from '../../models/job.model';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {

  displayedColumns = ['name', 'status', 'rating', 'createdDate'];
  dataSource = new JobDataSource(this.jobsService);

  constructor(
    private jobsService: JobService, 
    private changeDetectorRefs: ChangeDetectorRef,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
  }

  openJob(job) {
    console.log("selec job: "+job)
    let dialogRef = this.dialog.open(JobDetailsComponent, {
      width: '1000px',
      height: '600px',
      minWidth: '300px',
      data:{
        isUpdating: true,
        jobId: job._id,
        name: job.name,
        jobDescription: job.jobDescription,
        createdDate: job.createdDate,
        feedback: job.feedback,
        selectedType: job.status
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log("update status window closed")
      if (result != undefined) {
        console.log("update status window closed")
        // this.jobsService.editJob(result).subscribe(data => {
        //   console.log('edit job done and call back came' + JSON.stringify(data))
        //   window.alert(data.message)
        // })

        this.jobsService.editJob(result).subscribe(data => {
          window.alert(data.message)
          this.refresh()
        })
      }
    });
  }

  refresh() {
    this.dataSource = new JobDataSource(this.jobsService);
    this.changeDetectorRefs.detectChanges();
  }
}

export class JobDataSource extends DataSource<any> {
  
  public allProjects : Job[];
  
  constructor(
    private jobsService: JobService, 
  ) {
    super();
  }

  connect(): Observable<Job[]> {
    this.jobsService.getAllJobs().subscribe(data => {
      this.allProjects = data;
      console.log(data)
    });
    return this.jobsService.getAllJobs()
  }

  getProjectCount() {
    return this.allProjects
  }

  disconnect() {

  }
}
