import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { JobService } from '../../services/job.service';
import { Observable } from 'rxjs/Observable'
import { Job } from '../../models/job.model';
import { DataSource } from '@angular/cdk/collections';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  constructor(
    private jobService: JobService, 
    private authService: AuthService
  ) { }
  
  user: any
  ratedJobs: any

  // displayedColumns = ['name', 'status', 'rating', 'createdDate'];
  // dataSource = new JobDataSource(this.jobService);

  ngOnInit() {
    this.authService.getProfile().subscribe(profile => {
      this.user = profile.user
    }, err => {
      console.log(err)
      return false 
    })

    this.jobService.getAllRatedJobs().subscribe(jobs => {
      this.ratedJobs = jobs
      console.log(this.ratedJobs)
    }, err => {
      console.log(err)
      return false 
    })
  }

}


// export class JobDataSource extends DataSource<any> {
  
//   public allJobs : Job[];
  
//   constructor(
//     private jobsService: JobService, 
//   ) {
//     super();
//   }

//   connect(): Observable<Job[]> {
    

//     return this.jobsService.getAllRatedJobs()
//   }

//   getProjectCount() {
//     return this.allJobs
//   }

//   disconnect() {

//   }
// }