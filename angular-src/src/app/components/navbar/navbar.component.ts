import { Component, OnInit } from '@angular/core';
import { MaterialModule } from '../../material.module';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { tokenNotExpired } from 'angular2-jwt';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(
    public authService: AuthService, 
    private router: Router
  ) { }

  user: Object

  ngOnInit() {
  }

  onLogoutClick() {
    this.authService.logout()
    // window.alert('You are now logged out')
    this.router.navigate(['/login'])
  }

}
