import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { } from '../../material.module';
import { ValidateService } from '../../services/validate.service';

@Component({
  selector: 'app-job-details',
  templateUrl: './job-details.component.html',
  styleUrls: ['./job-details.component.css']
})
export class JobDetailsComponent {

  name: String
  jobDescription: String
  jobId: any
  createdDate: String
  feedback: String
  isUpdating = false
  isUpdatable = true
  selectedType: String
  types = [
    {value: 'Awaiting', viewValue: 'Awaiting'},
    {value: 'In Progress', viewValue: 'In Progress'},
    {value: 'Done', viewValue: 'Done'}
  ];

  constructor(public dialogRef: MatDialogRef<JobDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { 
      if (data.isUpdating) {

        if (this.selectedType == "Done") {
          this.isUpdatable = false
        } else {
          this.isUpdatable = true
        }

        this.name = data.name
        this.jobDescription = data.jobDescription
        this.isUpdating = data.isUpdating
        this.jobId = data.jobId
        this.feedback = data.feedback
        this.createdDate = data.createdDate
        this.selectedType = data.selectedType

        
        console.log('id: ' + this.isUpdatable)
      } 
    }

  ngOnInit() {
    console.log('ngOnInit: ' + this.isUpdatable)
  }

  update() {
    const updateData = {
      _id: this.jobId,
      name: this.name,
      jobDescription: this.jobDescription,
      status: this.selectedType
    }
    console.log(this.selectedType)
    this.dialogRef.close(updateData);
  }

  cancel() {
    this.dialogRef.close();
  }

}
