import { Component, OnInit } from '@angular/core';
import { ValidateService } from '../../services/validate.service';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email: string
  password: string

  constructor(
    private validateService: ValidateService, 
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  onLogin() {
    const login = {
      username: this.email,
      password: this.password
    }
    if (this.validateService.validateLoginDetails(login)) {
      // console.log('pwd1: '+login.password)
      this.authService.loginUser(login).subscribe(data => {
        // console.log('pwd: '+login.password)
        if (data.success) {
          this.authService.storeUserData(data.token, data.user)
          this.router.navigate(['/'])
        } else {
          window.alert('Login failed')
        }
      })
    }
  }
}
