const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors'); 
const passport = require('passport/lib');
const mongoose = require('mongoose');
const config = require('./config/database');
var session = require('express-session')

// Connect to database
mongoose.connect(config.database);

// On connection
mongoose.connection.on('connected', () => {
    console.log(`Connected to database ${config.database}`);
});

// DB connection error
mongoose.connection.on('error', (err) => {
    console.log(`Database error: ${err}`);
});

const app = express();
// const port = process.env.PORT || 8080 //3000;
const port = 3000;
// const port = 8080;

// adding users router class
const users = require('./routes/users')
const technicians = require('./routes/technicians')
const jobs = require('./routes/jobs')

// CORS middleware
app.use(cors()); 

// Passport middleware
app.use(passport.initialize());
app.use(passport.session());
app.use(session({
    secret: config.secret,
    resave: true,
    saveUninitialized: false
}))

require('./config/passport')(passport);

// Set static public folder
app.use(express.static(path.join(__dirname, 'public')));

// Body Parser middleware
app.use(bodyParser.json());

// users router class
app.use('/users', users);
app.use('/technicians', technicians);
app.use('/jobs', jobs);

app.use(app.route)

app.listen(port, () => {
    console.log(`Server started on port ${port}`);
});

app.get('/', (req, res) => {
    // res.send('Invalid endpoint');
    res.json({"message":"Invalid endpoint"});
});

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'public/index.html'))
})