webpackJsonp(["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<app-navbar></app-navbar>\n<div class=\"container\">\n  <router-outlet></router-outlet>\n</div>\n<!-- <div class=\"footer\">\n    <app-footer-view></app-footer-view>\n</div> -->\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app works!';
    }
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-root',
            template: __webpack_require__("./src/app/app.component.html"),
            styles: [__webpack_require__("./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__material_module__ = __webpack_require__("./src/app/material.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_platform_browser_animations__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/animations.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__components_navbar_navbar_component__ = __webpack_require__("./src/app/components/navbar/navbar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__components_login_login_component__ = __webpack_require__("./src/app/components/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__components_register_register_component__ = __webpack_require__("./src/app/components/register/register.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__components_home_home_component__ = __webpack_require__("./src/app/components/home/home.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__components_footer_view_footer_view_component__ = __webpack_require__("./src/app/components/footer-view/footer-view.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_ngx_google_places_autocomplete__ = __webpack_require__("./node_modules/ngx-google-places-autocomplete/bundles/ngx-google-places-autocomplete.umd.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_ngx_google_places_autocomplete___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_14_ngx_google_places_autocomplete__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__guards_auth_guard__ = __webpack_require__("./src/app/guards/auth.guard.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__services_validate_service__ = __webpack_require__("./src/app/services/validate.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__services_auth_service__ = __webpack_require__("./src/app/services/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__components_simple_dialog_simple_dialog_component__ = __webpack_require__("./src/app/components/simple-dialog/simple-dialog.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__components_notifications_notifications_component__ = __webpack_require__("./src/app/components/notifications/notifications.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__services_job_service__ = __webpack_require__("./src/app/services/job.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__components_job_details_job_details_component__ = __webpack_require__("./src/app/components/job-details/job-details.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






// Material
















var appRoutes = [
    { path: '', component: __WEBPACK_IMPORTED_MODULE_12__components_home_home_component__["a" /* HomeComponent */] },
    { path: 'register', component: __WEBPACK_IMPORTED_MODULE_11__components_register_register_component__["a" /* RegisterComponent */] },
    { path: 'login', component: __WEBPACK_IMPORTED_MODULE_10__components_login_login_component__["a" /* LoginComponent */] },
    { path: 'jobs', component: __WEBPACK_IMPORTED_MODULE_19__components_notifications_notifications_component__["a" /* NotificationsComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_15__guards_auth_guard__["a" /* AuthGuard */]] },
    { path: 'jobdetail', component: __WEBPACK_IMPORTED_MODULE_21__components_job_details_job_details_component__["a" /* JobDetailsComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_15__guards_auth_guard__["a" /* AuthGuard */]] },
    { path: 'job/:id/edit', component: __WEBPACK_IMPORTED_MODULE_21__components_job_details_job_details_component__["a" /* JobDetailsComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_15__guards_auth_guard__["a" /* AuthGuard */]] },
];
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_9__components_navbar_navbar_component__["a" /* NavbarComponent */],
                __WEBPACK_IMPORTED_MODULE_10__components_login_login_component__["a" /* LoginComponent */],
                __WEBPACK_IMPORTED_MODULE_11__components_register_register_component__["a" /* RegisterComponent */],
                __WEBPACK_IMPORTED_MODULE_12__components_home_home_component__["a" /* HomeComponent */],
                __WEBPACK_IMPORTED_MODULE_18__components_simple_dialog_simple_dialog_component__["a" /* SimpleDialogComponent */],
                __WEBPACK_IMPORTED_MODULE_13__components_footer_view_footer_view_component__["a" /* FooterViewComponent */],
                __WEBPACK_IMPORTED_MODULE_19__components_notifications_notifications_component__["a" /* NotificationsComponent */],
                __WEBPACK_IMPORTED_MODULE_21__components_job_details_job_details_component__["a" /* JobDetailsComponent */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_http__["HttpModule"],
                __WEBPACK_IMPORTED_MODULE_5__angular_router__["b" /* RouterModule */].forRoot(appRoutes),
                __WEBPACK_IMPORTED_MODULE_7__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
                __WEBPACK_IMPORTED_MODULE_6__material_module__["a" /* MaterialModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_14_ngx_google_places_autocomplete__["GooglePlaceModule"]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_16__services_validate_service__["a" /* ValidateService */],
                __WEBPACK_IMPORTED_MODULE_17__services_auth_service__["a" /* AuthService */],
                __WEBPACK_IMPORTED_MODULE_15__guards_auth_guard__["a" /* AuthGuard */],
                __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_20__services_job_service__["a" /* JobService */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/components/footer-view/footer-view.component.css":
/***/ (function(module, exports) {

module.exports = ".footer {\n    position: fixed;\n    left: 0;\n    bottom: 0;\n    width: 100%;\n    height: 130px;\n    background-color: black;\n    color: white;\n    text-align: center;\n }"

/***/ }),

/***/ "./src/app/components/footer-view/footer-view.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"footer\">\n  <p>All rights reserved @2018 SHU AAF</p><br>\n  <p>Developed by Himal Madhushan - 27045231</p>\n</div>"

/***/ }),

/***/ "./src/app/components/footer-view/footer-view.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FooterViewComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FooterViewComponent = /** @class */ (function () {
    function FooterViewComponent() {
    }
    FooterViewComponent.prototype.ngOnInit = function () {
    };
    FooterViewComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-footer-view',
            template: __webpack_require__("./src/app/components/footer-view/footer-view.component.html"),
            styles: [__webpack_require__("./src/app/components/footer-view/footer-view.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], FooterViewComponent);
    return FooterViewComponent;
}());



/***/ }),

/***/ "./src/app/components/home/home.component.css":
/***/ (function(module, exports) {

module.exports = "body,h1,h2,h3,h4,h5,h6 {font-family: \"Lato\", sans-serif;}\nbody, html {\n    height: 100%;\n    color: #777;\n    line-height: 1.8;\n}\n/* Create a Parallax Effect */\n.bgimg-1, .bgimg-2, .bgimg-3 {\n    background-attachment: fixed;\n    background-position: center;\n    background-repeat: no-repeat;\n    background-size: cover;\n}\n/* First image (Logo. Full height) */\n.bgimg-1 {\n    min-height: 100%;\n}\n/* Second image (Portfolio) */\n.bgimg-2 {\n    min-height: 400px;\n}\n/* Third image (Contact) */\n.bgimg-3 {\n    min-height: 400px;\n}\n.w3-wide {letter-spacing: 10px;}\n.w3-hover-opacity {cursor: pointer;}\n/* Turn off parallax scrolling for tablets and phones */\n@media only screen and (max-width: 100%) {\n    .bgimg-1, .bgimg-2, .bgimg-3 {\n        background-attachment: scroll;\n    }\n}"

/***/ }),

/***/ "./src/app/components/home/home.component.html":
/***/ (function(module, exports) {

module.exports = "<!DOCTYPE html>\n<html>\n<title>Auto Doc</title>\n<link rel=\"stylesheet\" href=\"https://www.w3schools.com/w3css/4/w3.css\">\n<link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/css?family=Lato\">\n\n<body>\n\n  <!-- First Parallax Image with Logo Text -->\n  <div class=\"bgimg-1\" id=\"home\">\n    <div class=\"w3-display-middle\" style=\"white-space:nowrap;\">\n      <span class=\"w3-center w3-padding-large w3-black w3-xlarge w3-wide w3-animate-opacity\">\n        <span class=\"w3-hide-small\">Auto Doc</span></span>\n    </div>\n  </div>\n\n  <script>\n    // Change style of navbar on scroll\n    window.onscroll = function () { myFunction() };\n    function myFunction() {\n      var navbar = document.getElementById(\"myNavbar\");\n      if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {\n        navbar.className = \"w3-bar\" + \" w3-card\" + \" w3-animate-top\" + \" w3-white\";\n      } else {\n        navbar.className = navbar.className.replace(\" w3-card w3-animate-top w3-white\", \"\");\n      }\n    }\n  </script>\n\n</body>\n</html>"

/***/ }),

/***/ "./src/app/components/home/home.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HomeComponent = /** @class */ (function () {
    function HomeComponent() {
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-home',
            template: __webpack_require__("./src/app/components/home/home.component.html"),
            styles: [__webpack_require__("./src/app/components/home/home.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/components/job-details/job-details.component.css":
/***/ (function(module, exports) {

module.exports = ".content-card {\n    max-width: 100%;\n    max-height: 100%;\n    min-width: 200px;\n    margin-top: 20px;\n}\n\n.content {\n    margin-bottom: 20px;\n}"

/***/ }),

/***/ "./src/app/components/job-details/job-details.component.html":
/***/ (function(module, exports) {

module.exports = "<mat-card class=\"content-card\" *ngIf=\"!isSharingView\">\n  <h2 mat-dialog-title>Update Job</h2>\n\n  <mat-dialog-content class=\"content\">\n\n    <mat-form-field style=\"width: 100%;\">\n      <input matInput placeholder=\"Job Name\" [(ngModel)]=\"name\" required>\n    </mat-form-field>\n\n    <mat-form-field style=\"width: 100%;\">\n      <textarea matInput placeholder=\"Job Description\" [(ngModel)]=\"jobDescription\" required></textarea>\n    </mat-form-field>\n\n    <mat-form-field style=\"width: 100%;\">\n      <input matInput placeholder=\"Created Date\" [(ngModel)]=\"createdDate\" disabled>\n    </mat-form-field>\n\n    <mat-form-field style=\"width: 100%;\">\n      <textarea matInput placeholder=\"Feedback\" [(ngModel)]=\"feedback\" disabled></textarea>\n    </mat-form-field>\n\n  </mat-dialog-content>\n\n  <mat-form-field>\n    <mat-select placeholder=\"Status\" [(value)]=\"selectedType\">\n      <mat-option *ngFor=\"let type of types\" [value]=\"type.value\">\n        {{type.viewValue}}\n      </mat-option>\n    </mat-select>\n  </mat-form-field>\n\n  <mat-dialog-actions>\n    <button mat-button (click)=\"cancel()\">Cancel</button>\n    <button mat-raised-button color=\"primary\" (click)=\"update()\">Update</button>\n  </mat-dialog-actions>\n\n</mat-card>"

/***/ }),

/***/ "./src/app/components/job-details/job-details.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return JobDetailsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var JobDetailsComponent = /** @class */ (function () {
    function JobDetailsComponent(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.isUpdating = false;
        this.types = [
            { value: 'Awaiting', viewValue: 'Awaiting' },
            { value: 'In Progress', viewValue: 'In Progress' },
            { value: 'Done', viewValue: 'Done' }
        ];
        if (data.isUpdating) {
            this.name = data.name;
            this.jobDescription = data.jobDescription;
            this.isUpdating = data.isUpdating;
            this.jobId = data.jobId;
            this.feedback = data.feedback;
            this.createdDate = data.createdDate;
            this.selectedType = data.selectedType;
            console.log('id: ' + data.jobId);
        }
    }
    JobDetailsComponent.prototype.ngOnInit = function () {
    };
    JobDetailsComponent.prototype.update = function () {
        var updateData = {
            _id: this.jobId,
            name: this.name,
            jobDescription: this.jobDescription,
            status: this.selectedType
        };
        console.log(this.selectedType);
        this.dialogRef.close(updateData);
    };
    JobDetailsComponent.prototype.cancel = function () {
        this.dialogRef.close();
    };
    JobDetailsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-job-details',
            template: __webpack_require__("./src/app/components/job-details/job-details.component.html"),
            styles: [__webpack_require__("./src/app/components/job-details/job-details.component.css")]
        }),
        __param(1, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_1__angular_material__["a" /* MAT_DIALOG_DATA */])),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_material__["d" /* MatDialogRef */], Object])
    ], JobDetailsComponent);
    return JobDetailsComponent;
}());



/***/ }),

/***/ "./src/app/components/login/login.component.css":
/***/ (function(module, exports) {

module.exports = ".title {\n  font-size: 40px;\n  text-align: center;\n  margin-bottom: 30px;\n  width: 100%;\n  color: rgb(0, 9, 26);\n}\n\n.content-card {\n  /* max-width: 80%; */\n  max-width: 1000px;\n  min-width: 200px;\n  height: 350px;\n  margin-left: auto;\n  margin-right: auto;\n  margin-top: 40px;\n  -ms-flex-item-align: center;\n      -ms-grid-row-align: center;\n      align-self: center;\n  border-radius: 10px;\n  -ms-flex-line-pack: center;\n      align-content: center;\n}\n\n.text-field {\n  margin-top: 30px;\n  width: 100%;\n}\n\n.login-button {\n  width: 100%;\n  -ms-flex-item-align: center;\n      -ms-grid-row-align: center;\n      align-self: center;\n  margin-top: 40px;\n  text-align: center;\n}"

/***/ }),

/***/ "./src/app/components/login/login.component.html":
/***/ (function(module, exports) {

module.exports = "<mat-card class=\"content-card md-elevation-z3\">\n  <div class=\"title\">Login</div>\n    <span>\n        <mat-form-field class=\"text-field\">\n          <input matInput placeholder=\"Enter your username\" id=\"txtEmail\" [(ngModel)]=\"email\" name=\"email\">\n        </mat-form-field>\n    </span>\n    <span>\n        <mat-form-field class=\"text-field\">\n          <input matInput placeholder=\"Enter your password\" id=\"txtPassword\" [(ngModel)]=\"password\" name=\"password\" type=\"password\">\n        </mat-form-field>\n    </span>\n\n    <button mat-raised-button color=\"primary\" class=\"login-button\" (click)=\"onLogin()\"> Sign in </button>\n</mat-card>"

/***/ }),

/***/ "./src/app/components/login/login.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_validate_service__ = __webpack_require__("./src/app/services/validate.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_auth_service__ = __webpack_require__("./src/app/services/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LoginComponent = /** @class */ (function () {
    function LoginComponent(validateService, authService, router) {
        this.validateService = validateService;
        this.authService = authService;
        this.router = router;
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.onLogin = function () {
        var _this = this;
        var login = {
            username: this.email,
            password: this.password
        };
        if (this.validateService.validateLoginDetails(login)) {
            // console.log('pwd1: '+login.password)
            this.authService.loginUser(login).subscribe(function (data) {
                // console.log('pwd: '+login.password)
                if (data.success) {
                    _this.authService.storeUserData(data.token, data.user);
                    _this.router.navigate(['/']);
                }
                else {
                    window.alert('Login failed');
                }
            });
        }
    };
    LoginComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-login',
            template: __webpack_require__("./src/app/components/login/login.component.html"),
            styles: [__webpack_require__("./src/app/components/login/login.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_validate_service__["a" /* ValidateService */],
            __WEBPACK_IMPORTED_MODULE_2__services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/components/navbar/navbar.component.css":
/***/ (function(module, exports) {

module.exports = ".nav-spacer {\n    -webkit-box-flex: 1;\n        -ms-flex: 1 1 auto;\n            flex: 1 1 auto;\n    max-width: 100%;\n}\n\n.nav-button {\n    padding-left:40px;\n    padding-right: 40px;\n}\n"

/***/ }),

/***/ "./src/app/components/navbar/navbar.component.html":
/***/ (function(module, exports) {

module.exports = "<mat-toolbar color=\"warn\" role=\"header\" class=\"mat-elevation-z4\">\n  <span [routerLink]=\"['/']\">Auto Doc</span>\n\n  <!-- Left buttons -->\n  <button mat-button *ngIf=\"authService.loggedIn()\" class=\"nav-button\" [routerLink]=\"['/jobs']\"> Jobs </button>\n  \n  <!-- Flexible space -->\n  <span class=\"nav-spacer\"></span>\n\n  <button mat-button *ngIf=\"!authService.loggedIn()\" [routerLink]=\"['/login']\"> Login </button>\n  <button mat-button *ngIf=\"!authService.loggedIn()\" [routerLink]=\"['/register']\"> Register </button>\n\n  <!-- Right side button -->\n  <button mat-button *ngIf=\"authService.loggedIn()\" [matMenuTriggerFor]=\"menu\">\n    <i class=\"material-icons\"> account_circle </i>\n  </button>\n  <mat-menu #menu=\"matMenu\">\n    <div *ngIf=\"authService.loggedIn()\">\n      <!-- <button id=\"btnProfile\" mat-menu-item class=\"nav-link\"[routerLink]=\"['/profile']\">Profile</button> -->\n      <button id=\"btnLogout\" mat-menu-item class=\"nav-link\" (click)=\"onLogoutClick()\" href=\"#\">Logout</button>\n    </div>\n  </mat-menu>\n\n</mat-toolbar>"

/***/ }),

/***/ "./src/app/components/navbar/navbar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_auth_service__ = __webpack_require__("./src/app/services/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(authService, router) {
        this.authService = authService;
        this.router = router;
    }
    NavbarComponent.prototype.ngOnInit = function () {
    };
    NavbarComponent.prototype.onLogoutClick = function () {
        this.authService.logout();
        // window.alert('You are now logged out')
        this.router.navigate(['/login']);
    };
    NavbarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-navbar',
            template: __webpack_require__("./src/app/components/navbar/navbar.component.html"),
            styles: [__webpack_require__("./src/app/components/navbar/navbar.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */]])
    ], NavbarComponent);
    return NavbarComponent;
}());



/***/ }),

/***/ "./src/app/components/notifications/notifications.component.css":
/***/ (function(module, exports) {

module.exports = ".highlight {\n    background: rgb(218, 229, 230); /* green */\n  }\n  \n  .hoverable {\n    cursor: pointer;\n    min-height: 60px;\n  }\n  \n  .hoverable:hover {\n    background: rgba(231, 236, 236, 0.495);\n  }\n  \n  .hoverable:focus {\n    background: rgba(231, 236, 236, 0.495);\n  }\n  \n  .content-card {\n    max-width: 100%;\n    margin-left: 40px;\n    margin-right: 40px;\n    margin-top: 20px;\n  }\n  \n  .add-project-button {\n      margin-bottom: 20px;\n  }\n  \n  .example-header-image {\n    background-image: url('https://material.angular.io/assets/img/examples/shiba1.jpg');\n    background-size: cover;\n  }\n  \n  .table-container {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    max-height: 500px;\n    min-width: 300px;\n    overflow: auto;\n  }\n  \n  .cell-view-button {\n    -ms-flex-item-align: end;\n        align-self: flex-end;\n  }\n  \n  .cell-date {\n    color: darkslategray;\n  }\n  \n  .delete-button {\n    background-color: gray;\n  }"

/***/ }),

/***/ "./src/app/components/notifications/notifications.component.html":
/***/ (function(module, exports) {

module.exports = "<mat-card class=\"content-card\">\n   \n    <br>\n    \n      <mat-card-content>\n\n        <h1>Your Jobs</h1>\n        \n        <!-- class=\"mat-elevation-z8\" -->\n        <mat-table [dataSource]=\"dataSource\" class=\"mat-elevation-z8\">\n    \n          <ng-container matColumnDef=\"name\">\n            <mat-header-cell *matHeaderCellDef>\n              <b style=\"\">Name</b>\n            </mat-header-cell>\n            <mat-cell *matCellDef=\"let element\"> {{element.name}} </mat-cell>\n          </ng-container>\n    \n          <ng-container matColumnDef=\"status\">\n              <mat-header-cell *matHeaderCellDef>\n                <b>Status</b>\n              </mat-header-cell>\n              <mat-cell *matCellDef=\"let element\"> {{element.status}} </mat-cell>\n            </ng-container>\n    \n          <ng-container matColumnDef=\"rating\">\n            <mat-header-cell *matHeaderCellDef>\n              <b>Rating</b>\n            </mat-header-cell>\n            <mat-cell *matCellDef=\"let element\"> {{element.rating}} </mat-cell>\n          </ng-container>\n    \n          <ng-container matColumnDef=\"createdDate\">\n            <mat-header-cell *matHeaderCellDef mat-sort-header>\n              <b>Created Date</b>\n            </mat-header-cell>\n            <mat-cell *matCellDef=\"let element\"> {{element.createdDate}} </mat-cell>\n          </ng-container>\n    \n          <!-- <ng-container matColumnDef=\"action\">\n            <mat-header-cell *matHeaderCellDef>\n              <b>Action</b>\n            </mat-header-cell>\n            <mat-cell *matCellDef=\"let element\">\n              <button mat-button >Delete</button>\n              <button mat-button >Share</button>\n              <button mat-button >Edit</button>\n            </mat-cell>\n          </ng-container> -->\n    \n          <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\n          <mat-row *matRowDef=\"let row; columns: displayedColumns;\" (click)=\"openJob(row)\" class=\"hoverable\"></mat-row>\n        </mat-table>\n    \n    \n      </mat-card-content>\n    \n    \n    </mat-card>"

/***/ }),

/***/ "./src/app/components/notifications/notifications.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationsComponent; });
/* unused harmony export JobDataSource */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_cdk_collections__ = __webpack_require__("./node_modules/@angular/cdk/esm5/collections.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__job_details_job_details_component__ = __webpack_require__("./src/app/components/job-details/job-details.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_job_service__ = __webpack_require__("./src/app/services/job.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_observable_of__ = __webpack_require__("./node_modules/rxjs/_esm5/add/observable/of.js");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var NotificationsComponent = /** @class */ (function () {
    function NotificationsComponent(jobsService, router, dialog) {
        this.jobsService = jobsService;
        this.router = router;
        this.dialog = dialog;
        this.displayedColumns = ['name', 'status', 'rating', 'createdDate'];
        this.dataSource = new JobDataSource(this.jobsService);
    }
    NotificationsComponent.prototype.ngOnInit = function () {
    };
    NotificationsComponent.prototype.openJob = function (job) {
        var _this = this;
        console.log("selec job: " + job);
        var dialogRef = this.dialog.open(__WEBPACK_IMPORTED_MODULE_3__job_details_job_details_component__["a" /* JobDetailsComponent */], {
            width: '1000px',
            height: '600px',
            minWidth: '300px',
            data: {
                isUpdating: true,
                jobId: job._id,
                name: job.name,
                jobDescription: job.jobDescription,
                createdDate: job.createdDate,
                feedback: job.feedback,
                selectedType: job.status
            }
        });
        dialogRef.afterClosed().subscribe(function (result) {
            console.log("update status window closed");
            if (result != undefined) {
                console.log("update status window closed");
                // this.jobsService.editJob(result).subscribe(data => {
                //   console.log('edit job done and call back came' + JSON.stringify(data))
                //   window.alert(data.message)
                // })
                _this.jobsService.editJob(result).subscribe(function (data) {
                    window.alert(data.message);
                });
            }
            // this.refresh()
        });
    };
    NotificationsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-notifications',
            template: __webpack_require__("./src/app/components/notifications/notifications.component.html"),
            styles: [__webpack_require__("./src/app/components/notifications/notifications.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__services_job_service__["a" /* JobService */],
            __WEBPACK_IMPORTED_MODULE_5__angular_router__["a" /* Router */],
            __WEBPACK_IMPORTED_MODULE_2__angular_material__["c" /* MatDialog */]])
    ], NotificationsComponent);
    return NotificationsComponent;
}());

var JobDataSource = /** @class */ (function (_super) {
    __extends(JobDataSource, _super);
    function JobDataSource(jobsService) {
        var _this = _super.call(this) || this;
        _this.jobsService = jobsService;
        return _this;
    }
    JobDataSource.prototype.connect = function () {
        var _this = this;
        this.jobsService.getAllJobs().subscribe(function (data) {
            _this.allProjects = data;
            console.log(data);
        });
        return this.jobsService.getAllJobs();
    };
    JobDataSource.prototype.getProjectCount = function () {
        return this.allProjects;
    };
    JobDataSource.prototype.disconnect = function () {
    };
    return JobDataSource;
}(__WEBPACK_IMPORTED_MODULE_1__angular_cdk_collections__["a" /* DataSource */]));



/***/ }),

/***/ "./src/app/components/register/register.component.css":
/***/ (function(module, exports) {

module.exports = ".title {\n  font-size: 40px;\n  text-align: center;\n  margin-bottom: 30px;\n  width: 100%;\n  color: rgb(0, 9, 26);\n}\n\n.content-card {\n  max-width: 1000px;\n  min-width: 200px;\n  /* height: 450px; */\n  margin-left: auto;\n  margin-right: auto;\n  margin-top: 40px;\n  -ms-flex-item-align: center;\n      -ms-grid-row-align: center;\n      align-self: center;\n  border-radius: 10px;\n}\n\n.text-field {\n  margin-top: 30px;\n  width: 100%;\n}\n\n.reg-button {\n  width: 100%;\n  margin-top: 40px;\n  text-align: center;\n}"

/***/ }),

/***/ "./src/app/components/register/register.component.html":
/***/ (function(module, exports) {

module.exports = "<mat-card class=\"content-card\">\n    <div class=\"title\">Register Technician</div>\n    <span>\n        <mat-form-field class=\"text-field\">\n            <input matInput placeholder=\"Enter your name (Ex: Sanduni Sachintha)\" id=\"txtName\" [(ngModel)]=\"name\" name=\"name\">\n        </mat-form-field>\n    </span>\n    <span>\n        <mat-form-field class=\"text-field\">\n            <input matInput placeholder=\"Enter a username for login\" id=\"txtEmail\" [(ngModel)]=\"username\" name=\"username\">\n        </mat-form-field>\n    </span>\n    <span>\n        <mat-form-field class=\"text-field\">\n            <input matInput placeholder=\"Enter a password for your account\" id=\"txtPassword\" [(ngModel)]=\"password\" name=\"password\" type=\"password\">\n        </mat-form-field>\n    </span>\n    <span>\n        <mat-form-field class=\"text-field\">\n            <input matInput ngx-google-places-autocomplete placesRef=\"ngx-places\" placeholder=\"Enter your address\" id=\"txtPassword\" [(ngModel)]=\"address\" name=\"address\" (onAddressChange)=\"onAddressChange($event)\">\n            <!-- <input ngx-google-places-autocomplete #placesRef=\"ngx-places\" (onAddressChange)=\"onAddressChange($event)\"/> -->\n        </mat-form-field>\n    </span>\n    <span>\n        <mat-form-field class=\"text-field\">\n            <input matInput placeholder=\"Enter your contact number\" id=\"txtPassword\" [(ngModel)]=\"phone\" name=\"phone\" >\n        </mat-form-field>\n    </span>\n\n    <span>\n    <mat-form-field>\n        <mat-select placeholder=\"Specialized in?\" [(value)]=\"selectedType\">\n          <mat-option *ngFor=\"let type of types\" [value]=\"type.value\">\n            {{type.viewValue}}\n          </mat-option>\n        </mat-select>\n      </mat-form-field>\n    </span>\n    <button mat-raised-button color=\"primary\" class=\"reg-button\" (click)=\"onRegisterSubmit()\"> Register Me </button>\n</mat-card>"

/***/ }),

/***/ "./src/app/components/register/register.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_validate_service__ = __webpack_require__("./src/app/services/validate.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_auth_service__ = __webpack_require__("./src/app/services/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// import { FlashMessagesService } from 'angular2-flash-messages';
var RegisterComponent = /** @class */ (function () {
    function RegisterComponent(vaidateService, authService, router) {
        this.vaidateService = vaidateService;
        this.authService = authService;
        this.router = router;
        this.types = [
            { value: 'Battery', viewValue: 'Battery' },
            { value: 'Tyre', viewValue: 'Tyre' },
            { value: 'Painting', viewValue: 'Painting' },
            { value: 'Engine Reparing', viewValue: 'Engine Reparing' },
            { value: 'Air Conditioning', viewValue: 'Air Conditioning' },
        ];
    }
    RegisterComponent.prototype.ngOnInit = function () {
        console.log("It's new year season. Happy new year!");
    };
    RegisterComponent.prototype.onAddressChange = function (address) {
        console.log(address.geometry.location.lng());
        console.log(address.geometry.location.lat());
        console.log(address.geometry.location.toJSON());
        console.log(address.geometry.viewport.getNorthEast());
        // this.address = address.adr_address
        this.lat = address.geometry.location.lat();
        this.lon = address.geometry.location.lng();
    };
    RegisterComponent.prototype.onRegisterSubmit = function () {
        var _this = this;
        var user = {
            name: this.name,
            username: this.username,
            password: this.password,
            phone: this.phone,
            address: this.address,
            type: this.selectedType,
            lat: this.lat,
            lon: this.lon
        };
        // required fields
        if (!this.vaidateService.validateRegisterDetails(user)) {
            window.alert('Please fill in all fields');
            return false;
        }
        // register user
        this.authService.registerUser(user).subscribe(function (data) {
            if (data.success) {
                window.alert('Register success');
                _this.router.navigate(['/login']);
            }
            else {
                window.alert('Register failed');
            }
        });
    };
    RegisterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-register',
            template: __webpack_require__("./src/app/components/register/register.component.html"),
            styles: [__webpack_require__("./src/app/components/register/register.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_validate_service__["a" /* ValidateService */],
            __WEBPACK_IMPORTED_MODULE_2__services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */]])
    ], RegisterComponent);
    return RegisterComponent;
}());



/***/ }),

/***/ "./src/app/components/simple-dialog/simple-dialog.component.css":
/***/ (function(module, exports) {

module.exports = ".content-card {\n    max-width: 100%;\n    max-height: 100%;\n    min-width: 200px;\n    margin-top: 20px;\n}\n\n.content {\n    margin-bottom: 20px;\n}"

/***/ }),

/***/ "./src/app/components/simple-dialog/simple-dialog.component.html":
/***/ (function(module, exports) {

module.exports = "<mat-card class=\"content-card\" *ngIf=\"!isSharingView\">\n  <h2 mat-dialog-title>Add New Project</h2>\n\n  <mat-dialog-content class=\"content\">\n\n    <mat-form-field style=\"width: 100%;\">\n      <input matInput placeholder=\"Enter Project Name\" [(ngModel)]=\"name\" required>\n    </mat-form-field>\n\n    <mat-form-field style=\"width: 100%;\">\n      <input matInput placeholder=\"Enter Project Code\" [(ngModel)]=\"projectCode\" required>\n    </mat-form-field>\n\n    <mat-form-field style=\"width: 100%;\">\n      <textarea matInput placeholder=\"Project Description\" [(ngModel)]=\"projectDesciption\" required></textarea>\n    </mat-form-field>\n\n  </mat-dialog-content>\n\n  <mat-dialog-actions *ngIf=\"!isUpdating\">\n    <button mat-raised-button color=\"primary\" (click)=\"done()\">Done</button>\n  </mat-dialog-actions>\n  <mat-dialog-actions *ngIf=\"isUpdating\">\n    <button mat-raised-button color=\"primary\" (click)=\"update()\">Update</button>\n  </mat-dialog-actions>\n</mat-card>\n\n\n<mat-card *ngIf=\"isSharingView\">\n  <h2 mat-dialog-title>Add Users</h2>\n  <mat-dialog-content>\n    <mat-form-field style=\"width: 100%;\">\n      <mat-select [(value)]=\"assignedUser\" placeholder=\"Select user\">\n        <mat-option *ngFor=\"let user of allUsers\" [value]=\"user\">\n          {{ user.name }}\n        </mat-option>\n      </mat-select>\n    </mat-form-field>\n  </mat-dialog-content>\n  <mat-dialog-actions>\n    <button mat-raised-button color=\"primary\" (click)=\"addUser()\">Add</button>\n  </mat-dialog-actions>\n</mat-card>"

/***/ }),

/***/ "./src/app/components/simple-dialog/simple-dialog.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SimpleDialogComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_validate_service__ = __webpack_require__("./src/app/services/validate.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var SimpleDialogComponent = /** @class */ (function () {
    function SimpleDialogComponent(dialogRef, data, validateService) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.validateService = validateService;
        this.isUpdating = false;
        this.isSharingView = false;
        if (data.isUpdating) {
            this.name = data.name;
            this.projectCode = data.projectCode;
            this.projectDesciption = data.projectDesciption;
            this.isUpdating = data.isUpdating;
            this.projectId = data.projectId;
            console.log('id: ' + data.projectId);
        }
        else if (data.isSharingView) {
            this.isSharingView = data.isSharingView;
            this.allUsers = data.allUsers;
        }
    }
    SimpleDialogComponent.prototype.done = function () {
        var newProjData = {
            name: this.name,
            projectDescription: this.projectDesciption,
            projectCode: this.projectCode
        };
        // console.log(newProjData)
        if (this.validateService.validateAddProjectDetails(newProjData)) {
            this.dialogRef.close(newProjData);
        }
    };
    SimpleDialogComponent.prototype.update = function () {
        var updateData = {
            _id: this.projectId,
            name: this.name,
            projectDescription: this.projectDesciption,
            projectCode: this.projectCode
        };
        if (this.validateService.validateAddProjectDetails(updateData)) {
            // console.log(updateData)
            this.dialogRef.close(updateData);
        }
    };
    SimpleDialogComponent.prototype.addUser = function () {
        console.log(this.assignedUser);
        var updateData = {
            userId: this.assignedUser._id,
            userName: this.assignedUser.name
        };
        if (this.assignedUser != undefined) {
            this.dialogRef.close(updateData);
        }
    };
    SimpleDialogComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-simple-dialog',
            template: __webpack_require__("./src/app/components/simple-dialog/simple-dialog.component.html"),
            styles: [__webpack_require__("./src/app/components/simple-dialog/simple-dialog.component.css")]
        }),
        __param(1, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_1__angular_material__["a" /* MAT_DIALOG_DATA */])),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_material__["d" /* MatDialogRef */], Object, __WEBPACK_IMPORTED_MODULE_2__services_validate_service__["a" /* ValidateService */]])
    ], SimpleDialogComponent);
    return SimpleDialogComponent;
}());



/***/ }),

/***/ "./src/app/guards/auth.guard.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthGuard; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_auth_service__ = __webpack_require__("./src/app/services/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AuthGuard = /** @class */ (function () {
    function AuthGuard(authService, router) {
        this.authService = authService;
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function () {
        if (this.authService.loggedIn()) {
            return true;
        }
        else {
            this.router.navigate(['/login']);
            return false;
        }
    };
    AuthGuard = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/material.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MaterialModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_material_toolbar__ = __webpack_require__("./node_modules/@angular/material/esm5/toolbar.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_material_menu__ = __webpack_require__("./node_modules/@angular/material/esm5/menu.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_material_card__ = __webpack_require__("./node_modules/@angular/material/esm5/card.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_material_form_field__ = __webpack_require__("./node_modules/@angular/material/esm5/form-field.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_material_icon__ = __webpack_require__("./node_modules/@angular/material/esm5/icon.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_material_input__ = __webpack_require__("./node_modules/@angular/material/esm5/input.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_material_grid_list__ = __webpack_require__("./node_modules/@angular/material/esm5/grid-list.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_material_dialog__ = __webpack_require__("./node_modules/@angular/material/esm5/dialog.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_material_table__ = __webpack_require__("./node_modules/@angular/material/esm5/table.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_material_list__ = __webpack_require__("./node_modules/@angular/material/esm5/list.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_material_divider__ = __webpack_require__("./node_modules/@angular/material/esm5/divider.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__angular_material_expansion__ = __webpack_require__("./node_modules/@angular/material/esm5/expansion.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__angular_material_datepicker__ = __webpack_require__("./node_modules/@angular/material/esm5/datepicker.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__angular_material_select__ = __webpack_require__("./node_modules/@angular/material/esm5/select.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








// import { MatSnackBarModule } from '@angular/material/snack-bar'








// import { MatSnackBar } from '@angular/material/snack-bar';
var MaterialModule = /** @class */ (function () {
    function MaterialModule() {
    }
    MaterialModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_material__["b" /* MatButtonModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_material_toolbar__["a" /* MatToolbarModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material_menu__["a" /* MatMenuModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material_card__["a" /* MatCardModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material_form_field__["c" /* MatFormFieldModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_material_icon__["a" /* MatIconModule */],
                __WEBPACK_IMPORTED_MODULE_7__angular_material_input__["b" /* MatInputModule */],
                __WEBPACK_IMPORTED_MODULE_8__angular_material_grid_list__["a" /* MatGridListModule */],
                __WEBPACK_IMPORTED_MODULE_9__angular_material_dialog__["c" /* MatDialogModule */],
                __WEBPACK_IMPORTED_MODULE_10__angular_material_table__["a" /* MatTableModule */],
                __WEBPACK_IMPORTED_MODULE_12__angular_material_divider__["a" /* MatDividerModule */],
                __WEBPACK_IMPORTED_MODULE_11__angular_material_list__["a" /* MatListModule */],
                __WEBPACK_IMPORTED_MODULE_13__angular_material_expansion__["a" /* MatExpansionModule */],
                __WEBPACK_IMPORTED_MODULE_14__angular_material_datepicker__["a" /* MatDatepickerModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_material__["e" /* MatNativeDateModule */],
                __WEBPACK_IMPORTED_MODULE_15__angular_material_select__["a" /* MatSelectModule */],
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_material__["b" /* MatButtonModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_material_toolbar__["a" /* MatToolbarModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material_menu__["a" /* MatMenuModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material_card__["a" /* MatCardModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material_form_field__["c" /* MatFormFieldModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_material_icon__["a" /* MatIconModule */],
                __WEBPACK_IMPORTED_MODULE_7__angular_material_input__["b" /* MatInputModule */],
                __WEBPACK_IMPORTED_MODULE_8__angular_material_grid_list__["a" /* MatGridListModule */],
                __WEBPACK_IMPORTED_MODULE_9__angular_material_dialog__["c" /* MatDialogModule */],
                __WEBPACK_IMPORTED_MODULE_10__angular_material_table__["a" /* MatTableModule */],
                __WEBPACK_IMPORTED_MODULE_12__angular_material_divider__["a" /* MatDividerModule */],
                __WEBPACK_IMPORTED_MODULE_11__angular_material_list__["a" /* MatListModule */],
                __WEBPACK_IMPORTED_MODULE_13__angular_material_expansion__["a" /* MatExpansionModule */],
                __WEBPACK_IMPORTED_MODULE_14__angular_material_datepicker__["a" /* MatDatepickerModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_material__["e" /* MatNativeDateModule */],
                __WEBPACK_IMPORTED_MODULE_15__angular_material_select__["a" /* MatSelectModule */]
            ]
        })
    ], MaterialModule);
    return MaterialModule;
}());



/***/ }),

/***/ "./src/app/services/auth.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_jwt__ = __webpack_require__("./node_modules/angular2-jwt/angular2-jwt.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_jwt___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angular2_jwt__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AuthService = /** @class */ (function () {
    function AuthService(http, router) {
        this.http = http;
        this.router = router;
    }
    AuthService.prototype.registerUser = function (user) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/json');
        // return this.http.post('http://localhost:3000/technicians/registertechnician', user, {headers: headers}).map(res => res.json())
        return this.http.post('http://localhost:8080/technicians/registertechnician', user, { headers: headers }).map(function (res) { return res.json(); });
        // return this.http.post('technicians/registertechnician', user, {headers: headers}).map(res => res.json())
    };
    AuthService.prototype.loginUser = function (login) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/json');
        // return this.http.post('http://localhost:3000/technicians/authenticate', login, {headers: headers}).map(res => res.json())
        return this.http.post('http://localhost:8080/technicians/authenticate', login, { headers: headers }).map(function (res) { return res.json(); });
        // return this.http.post('technicians/authenticate', login, {headers: headers}).map(res => res.json())
    };
    AuthService.prototype.loadToken = function () {
        var token = localStorage.getItem('id_token'); //id_token
        this.authToken = token;
        this.user = this.user;
    };
    AuthService.prototype.storeUserData = function (token, user) {
        localStorage.setItem('id_token', token);
        localStorage.setItem('user', JSON.stringify(user));
        this.authToken = token;
        this.user = user;
    };
    AuthService.prototype.logout = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/json');
        this.http.post('logout', { headers: headers }).map(function (res) { return res.json(); });
        this.http.post('users/logout', { headers: headers }).map(function (res) { return res.json(); });
        this.http.post('http://localhost:3000/users/logout', { headers: headers }).map(function (res) { return res.json(); });
        this.authToken = null;
        this.user = null;
        localStorage.clear();
    };
    AuthService.prototype.loggedIn = function () {
        return Object(__WEBPACK_IMPORTED_MODULE_3_angular2_jwt__["tokenNotExpired"])('id_token');
    };
    AuthService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"], __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* Router */]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/services/job.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return JobService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__auth_service__ = __webpack_require__("./src/app/services/auth.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var JobService = /** @class */ (function () {
    function JobService(http, authService, httpClient) {
        this.http = http;
        this.authService = authService;
        this.httpClient = httpClient;
    }
    JobService.prototype.getAllJobs = function () {
        this.authService.loadToken();
        var httpOptions = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["c" /* HttpHeaders */]({
                'Content-Type': 'application/json',
                'Authorization': this.authService.authToken
            })
        };
        console.log("getting all jobs");
        // return this.httpClient.get<Job[]>('http://localhost:3000/technicians/alljobs', httpOptions)
        return this.httpClient.get('http://localhost:8080/technicians/alljobs', httpOptions);
        // return this.httpClient.get<Job[]>('technicians/alljobs', httpOptions)
    };
    JobService.prototype.editJob = function (job) {
        // console.log("ediding job id: "+job._id)
        this.authService.loadToken();
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Authorization', this.authService.authToken);
        headers.append('Content-Type', 'application/json');
        // return this.http.post('http://localhost:3000/technicians/updatejobstatus', job, {headers: headers}).map(res => res.json())
        return this.http.post('http://localhost:8080/technicians/updatejobstatus', job, { headers: headers }).map(function (res) { return res.json(); });
        // return this.http.post('technicians/updatejobstatus', job,{headers: headers}).map(res => res.json())
    };
    JobService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"],
            __WEBPACK_IMPORTED_MODULE_4__auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */]])
    ], JobService);
    return JobService;
}());



/***/ }),

/***/ "./src/app/services/validate.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ValidateService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ValidateService = /** @class */ (function () {
    function ValidateService() {
    }
    ValidateService.prototype.validateRegisterDetails = function (user) {
        if (user.name == undefined || user.username == undefined || user.password == undefined || user.address == undefined || user.phone == undefined) {
            return false;
        }
        else {
            return true;
        }
    };
    ValidateService.prototype.validateEmail = function (email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    };
    ValidateService.prototype.validateLoginDetails = function (login) {
        if (login.username == undefined || login.password == undefined) {
            window.alert('Please fill required fields');
            return false;
        }
        else {
            return true;
        }
    };
    ValidateService.prototype.validateAddProjectDetails = function (newProject) {
        if (newProject.name == undefined || newProject.projectDescription == undefined || newProject.projectCode == undefined) {
            return false;
        }
        else {
            return true;
        }
    };
    ValidateService.prototype.validateCreateSprint = function (sprint) {
        if (sprint.sprintName == undefined || sprint.sprintStartingDate == undefined || sprint.sprintEndDate == undefined) {
            return false;
        }
        else {
            return true;
        }
    };
    ValidateService.prototype.validateCreateTask = function (task) {
        if (task.name == undefined || task.taskDescription == undefined || task.assignedUser.userId == undefined || task.taskNumber == undefined) {
            return false;
        }
        else {
            return true;
        }
    };
    ValidateService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], ValidateService);
    return ValidateService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "./src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_environments_environment__ = __webpack_require__("./src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_app_app_module__ = __webpack_require__("./src/app/app.module.ts");




if (__WEBPACK_IMPORTED_MODULE_2_environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["enableProdMode"])();
}
Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_3_app_app_module__["a" /* AppModule */]);


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map