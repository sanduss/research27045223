const express = require('express')
const router = express.Router();
const Technician = require('../models/technician');
const User = require('../models/user');
const passport = require('passport/lib')
const jwt = require('jsonwebtoken')
const config = require('../config/database');
const Job = require('../models/job');
// var url = require('url');
// const passportConfig = require('../config/passport');


// User list
router.get('/', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    
    // var id = req.query.type
    // console.log(req.query.type)
    console.log("=================================================================")
    console.log("GET ALL TECHNITICIANS - TYPE: "+req.query.type)
    Technician.getAllUsers(req.query.type, (err, users) => {
        if (!err) {
            if (users.length > 0) {
                res.send(users)
            } else {
                res.json({ success: false, message: `No technicians under ${req.query.type}` })
            }
        } else {
            res.json({ success: false, message: "Unable to get user list" })
        }
    })
})

// Register
router.post('/registertechnician', (req, res, next) => {
    console.log("=================================================================")
    console.log("POST REGISTER TECHNICIAN - UN: "+req.body.username)
    let newUser = new User({
        name: req.body.name,
        username: req.body.username,
        password: req.body.password,
        phone: req.body.phone,
        address: req.body.address,
        type: req.body.type,
        isTechnician: true,
        location : {
            lat: req.body.lat,
            lon: req.body.lon
        }
    })
    console.log(newUser)
    Technician.addTechnician(newUser, (err, user) => {
        if (err) {
            res.json({ success: false, message: "User registration failed" })
        } else {
            res.json({ success: true, message: "User registration success" })
        }
    })
})

// Authenticate
router.post('/authenticate', (req, res, next) => {
    console.log("=================================================================")
    console.log("TECHNICIAN AUTHENTICATE")
    const username = (req.body.username);
    const password = (req.body.password);
    console.log('Email: '+username)
    Technician.getUserByUsername(username, (err, user) => {
        if (err) console.log('auth user error: ' + err)
        // console.log('User found: '+user);
        if (!user) {
            console.log("User not found")
            return res.json({ success: false, message: "User not found" })
        }

        if (!user.isTechnician) {
            console.log("User is not a technician")
            return res.json({ success: false, message: "User is not a technician" })
        }

        Technician.comparePassword(password, user.password, (err, isMatch) => {
            if (err) { console.log('Password match error: ' + err); }
            if (isMatch) {
                const token = jwt.sign(user.toJSON(), config.secret, {
                    expiresIn: 600000
                })
                res.json({
                    success: true,
                    token: 'JWT ' + token,
                    message: "",
                    user: {
                        id: user._id,
                        name: user.name,
                        username: user.username,
                        address: user.address,
                        phone: user.phone
                    }
                })
            } else {
                res.json({ success: false, message: "Wrong password" })
            }
        })
    })
})

// Profile
router.get('/profile', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    console.log("=================================================================")
    console.log("TECHNICIAN PROFILE")
    res.json({user: req.user})
})

// All jobs
router.get('/alljobs', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    console.log("=================================================================")
    console.log("GET ALL TECH JOBS")
    // res.send(req.user.jobs)
    Job.getAllTechnicianJobs(req.user, (err, jobs) => {
        if (!err) {
            res.send(jobs)
        } else {
            res.json({ success: false, message: "Unable to get job list" })
        }
    })
})

router.post('/updatejobstatus', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    console.log("=================================================================")
    console.log("POST UPDATE JOB - ID: "+req.body._id + " - " + req.body.status)
    const tobeUpdatedJob = {
        jobId: req.body._id,
        name: req.body.name,
        status: req.body.status,
        jobDescription: req.body.jobDescription,
    }
    Job.updateDetails(tobeUpdatedJob, (err, response) => {
        if (err) {
            res.json({ success: false, message: "Update job status failed with error: "+err })
        } else {
            res.json({ success: true, message: "Update job status done" })
        }
    })
})

// GET /logout
router.get('/logout', function(req, res, next) {
    if (req.session) {
      // delete session object
      req.session.destroy(function(err) {
        if(err) {
          return next(err);
        } else {
          return res.redirect('/login');
        }
      });
    }
  });

module.exports = router;