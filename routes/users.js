const express = require('express')
const router = express.Router();
const User = require('../models/user');
const passport = require('passport/lib')
const jwt = require('jsonwebtoken')
const config = require('../config/database');
const constants = require('../constants');
var session = require('express-session')
const Job = require('../models/job');
// const passportConfig = require('../config/passport');


// User list
router.get('/', (req, res, next) => {
    User.getAllUsers((err, users) => {
        if (!err) {
            res.send(users)
        } else {
            res.json({ success: false, message: "Unable to get user list" })
        }
    })
})

// Register
router.post('/registercustomer', (req, res, next) => {
    console.log("=================================================================")
    console.log("POST REGISTER CUSTOMER - UN: "+req.body.username)
    let newUser = new User({
        name: req.body.name,
        username: req.body.username,
        password: req.body.password,
        phone: req.body.phone,
        address: req.body.address,
        isTechnician: false
    })
    // console.log(newUser)
    User.addUser(newUser, (err, user) => {
        if (err) {
            res.json({ success: false, message: "User registration failed: "+err })
        } else {
            res.json({
                success: true, message: "User registration success", user: {
                    id: user._id,
                    name: user.name,
                    username: user.username,
                    address: user.address,
                    phone: user.phone,
                    jobs: user.jobs
                }
            })
        }
    })
})

router.get('/alljobs', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    // res.send(req.user.jobs)
    console.log("=================================================================")
    console.log("GET CUSTOMER HISTORY")
    Job.getAllUserJobs(req.user, (err, jobs) => {
        if (!err) {
            console.log(err)
            if (jobs.length > 0) {
                res.send(jobs)
            } else {
                res.json({ success: false, message: "No jobs available" })
            }
        } else {
            res.json({ success: false, message: "Unable to get job list" })
        }
    })
})

// Authenticate
router.post('/authenticate', (req, res, next) => {
    console.log("=================================================================")
    const username = req.body.username;
    const password = req.body.password;
    console.log("USER AUTHENTICATE")
    User.getUserByUsername(username, (err, user) => {
        if (err) { 
            console.log('auth user error: ' + err)
            return res.json({ success: false, message: "auth user error: " + err })
        }
        // console.log('User found: '+user);
        if (!user) {
            return res.json({ success: false, message: "User not found" })
        }

        User.comparePassword(password, user.password, (err, isMatch) => {
            if (err) { 
                console.log('Password match error: ' + err); 
            }
            if (isMatch) {
                const token = jwt.sign(user.toJSON(), config.secret, {
                    expiresIn: 600000
                })
                res.json({
                    success: true,
                    token: 'JWT ' + token,
                    message: "",
                    user: {
                        id: user._id,
                        name: user.name,
                        username: user.username,
                        address: user.address,
                        phone: user.phone,
                        jobs: user.jobs
                    }
                })
            } else {
                res.json({ success: false, message: "Wrong password" })
            }
        })
    })
})

// Profile
router.get('/profile', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    res.json({ user: req.user })
})

// GET /logout
router.get('/logout', function (req, res, next) {
    if (req.session) {
        // delete session object
        req.session.destroy(function (err) {
            if (err) {
                return next(err);
            } else {
                return res.redirect('/login');
            }
        });
    }
});
module.exports = router;