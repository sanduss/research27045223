const express = require('express')
const router = express.Router();
const Job = require('../models/job');
const Technician = require('../models/technician');
const passport = require('passport')

// Job list
router.get('/', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    console.log("=================================================================")
    console.log("GET ALL JOBS")
    Job.getAllJobs(req.user, (err, jobs) => {
        if (!err) {
            console.log(err)
            res.send(jobs)
        } else {
            res.json({ success: false, message: "Unable to get job list" })
        }
    })
})

router.post('/', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    console.log("=================================================================")
    console.log("POST JOB - JOB_NAME: "+req.body.name)
    let newJob = new Job({
        name: req.body.name,
        jobDescription: req.body.jobDescription,
        status: req.body.status,
        lat: req.body.lat,
        lon: req.body.lon,
        address: req.body.address
    })
    // reportingUser = {userId: req.body.reportingUser.userId, userName: req.body.reportingUser.userId}
    Job.insertJob(req.body.technicianId, req.user, res, newJob, (err, createdNewjob) => {
        if (!err) {
            res.send(createdNewjob)
        } else {
            res.json({ success: false, message: "Unable to get job list" })
        }
    })
})

router.put('/:id', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    console.log("=================================================================")
    console.log("UPDATE JOB FEEDBACK - JOB_ID: "+req.params.id)
    const { exec } = require('child_process');
    
        // exec("cd /home/ && export LC_ALL=C", (err2, stdout, stderr) => {
        //     if (!err2) {
        //         console.log("cd ../home")
        //         exec(`/bin/bash/source kerasvirtual/bin/activate`, (err3, stdout, stderr) => {
        //             if (!err3) {
        //                 console.log("/bin/bash/source kerasvirtual/bin/activate")
        //             } else {
        //                 console.log("source kerasvirtual/bin/activate ERROR: "+err3)
        //             }
        //             exec(`cd kerasvirtual`, (err4, stdout, stderr) => {
        //                 if (!err4) {
        //                     console.log("cd kerasvirtual")
        //                 }
        //                 exec(`python3 make_predictions.py "${req.body.feedback}"`, (err5, stdout, stderr) => {
        //                     if (!err5) {
        //                         console.log("python3 make_predictions.py")
        //                     }
        //                 })
        //             })
        //         })
        //     }
        // })

        console.log("======== FEEDBACK: "+req.body.feedback)

        if (req.body.feedback == undefined) {
            res.json({success: false, message: "Please send a feedback. Feedback should not be empty"})
            return;
        }

    // Ubuntu
    exec(`cd /home/kerasvirtual && . bin/activate && python make_predictions.py "${req.body.feedback}"`, (err, stdout, stderr) => {

    // MacOS
    // exec(`cd ~/mlvirtualenv && source ./bin/activate && python3 make_predictions.py "${req.body.feedback}"`, (err, stdout, stderr) => {
        if (err) {
            console.log("======== PYTHON ERR: "+err)
            res.json({success: false})
            return;
        } else {
            var words = stdout.split(' ');
            const feedbackRating = words[0]
            console.log("======== RATING: " +feedbackRating)

            const tobeUpdatedJob = {
                jobId: req.params.id,
                feedback: req.body.feedback,
                rating: feedbackRating
            }
            Job.updateFeedback(tobeUpdatedJob, (err, response) => {
                if (err) {
                    res.json({ success: false, message: "Update job feedback failed" })
                } else {
                    res.json({ success: true, message: "Feedback successfully submitted", response: response })
                    updateTechnicianRating(response.technician, tobeUpdatedJob.rating)
                }
            })
        }
    });
    
})

function updateTechnicianRating(techid, rating) {
    console.log("=================================================================")
    console.log("UPDATE TECH RATING - TECH_ID: "+techid)
    Job.getAllTechnicianJobsForRating(techid, (err, jobs) => {
        if (!err) {
            var jobCount = jobs.length
            var totalRating = 0.0000
            jobs.forEach(function(element) {
                
                var rr = element.rating.toFixed(4)
                // console.log("typeof rr: "+typeof(rr))
                // console.log("typeof totalRating: "+typeof(rr))
                // console.log("*** ELEMENT RATING: "+rr)
                totalRating = totalRating + +rr
                // console.log("1 TOTAL RATING: "+totalRating)
            });
            
            if (totalRating > 0.0) {
                // totalRating = +totalRating + +rating
                var newRate = (totalRating / jobCount.toFixed(4))
                console.log("TOTAL RATING: "+totalRating)
                console.log("NEW RATING: "+rating)
                console.log("JOB COUNT: "+jobCount)
                console.log("CALCULATED RATING: "+newRate.toString())
                if (isNaN(newRate)) {
                    
                }
                Technician.updateTechnicianRating(techid, newRate, (err, response) => {
                    if (!err) {
                        console.log("technician rating updated")
                    } else {
                        console.log("technician rating update failed")
                    }
                })
            } else {
                Technician.updateTechnicianRating(techid, rating, (err, response) => {
                    if (!err) {
                        console.log("first time technician rating updated")
                    } else {
                        console.log("technician rating update failed")
                    }
                })
            }
        } else {
            res.json({ success: false, message: "Unable to update technician rating" })
        }
    })
}

router.put('/:id/updatedetails', (req, res, next) => {
    console.log("=================================================================")
    console.log("UPDATE JOB: "+req.body._id,+" STATUS: "+req.body.status)
    const tobeUpdatedJob = {
        jobId: req.body._id,
        name: req.body.name,
        status: req.body.status,
        jobDescription: req.body.jobDescription,
    }
    console.log("JOB STATUS: " + req.body.status)
    Job.updateDetails(tobeUpdatedJob, (err, response) => {
        if (err) {
            res.json({ success: false, message: "Update job feedback failed with error: " + err })
        } else {
            res.json({ success: true, message: "Update job feedback done" })
        }
    })
})

router.get('/ratedjobs', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    console.log("=================================================================")
    console.log("GET TECH RATED ALL JOBS")
    Job.getRatedJobs(req.user, (err, jobs) => {
        if (!err) {
            console.log(err)
            res.send(jobs)
        } else {
            res.json({ success: false, message: "Unable to get job list" })
        }
    })
})

module.exports = router;